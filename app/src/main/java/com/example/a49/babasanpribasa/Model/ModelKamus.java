package com.example.a49.babasanpribasa.Model;

/**
 * Created by 49 on 06/03/2018.
 */

public class ModelKamus {
    private String Babas;
    private String Makna;


    public ModelKamus() {
    }

    public ModelKamus(String babas, String makna) {
        Babas = babas;
        Makna = makna;
    }

    public String getBabas() {
        return Babas;
    }

    public void setBabas(String babas) {
        Babas = babas;
    }

    public String getMakna() {
        return Makna;
    }

    public void setMakna(String makna) {
        Makna = makna;
    }
}
