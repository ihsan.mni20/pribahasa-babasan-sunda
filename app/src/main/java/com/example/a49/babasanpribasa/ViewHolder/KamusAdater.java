package com.example.a49.babasanpribasa.ViewHolder;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a49.babasanpribasa.Makna;
import com.example.a49.babasanpribasa.Model.ModelKamus;
import com.example.a49.babasanpribasa.ViewHolder.KamusAdater.KamusViewHolder;
import com.example.a49.babasanpribasa.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 49 on 06/03/2018.
 */



public class KamusAdater extends RecyclerView.Adapter<KamusViewHolder>{

    private  List<ModelKamus> listData;
    private static Context ctx;

    public KamusAdater(List<ModelKamus> listData, Context ctx){
        this.listData = listData;
        KamusAdater.ctx = ctx;

    }
    @Override
    public KamusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.kamus_layout,parent, false);
        KamusViewHolder kamusViewHolder = new KamusViewHolder(itemView);
        return kamusViewHolder;
    }

    @Override
    public void onBindViewHolder(KamusViewHolder holder, int position) {
        ModelKamus mk = listData.get(position);
        holder.txt_kamus.setText(listData.get(position).getBabas());
        holder.mk =mk;

    }
    @Override
    public int getItemCount() {
        return listData.size();
    }

    public static class KamusViewHolder extends RecyclerView.ViewHolder{

        public final TextView txt_kamus;
        ModelKamus mk;

        public KamusViewHolder(View itemView) {
            super(itemView);

            txt_kamus = itemView.findViewById(R.id.kamus_name);
            itemView.setOnClickListener(v -> {
                //Toast.makeText(v.getContext(), txt_kamus.getText().toString(), Toast.LENGTH_SHORT).show();
                Intent goInput = new Intent(ctx, Makna.class);
                goInput.putExtra("babas", mk.getBabas());
                goInput.putExtra("makna", mk.getMakna());
                ctx.startActivity(goInput);
            });
        }
    }


}
