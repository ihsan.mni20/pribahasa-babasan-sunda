package com.example.a49.babasanpribasa;


import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import android.widget.TextView;

import com.example.a49.babasanpribasa.Database.Database;
import com.example.a49.babasanpribasa.Model.ModelKuis;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import info.hoang8f.widget.FButton;


public class Kuis extends AppCompatActivity {

    private FButton btnA, btnB, btnC, btnD;
    Button btnSelesai;
    List<ModelKuis> listSoal = new ArrayList<>();
    private int[] jawabanYgDiPilih;
    private int[] jawabanYgBenar;
    private boolean cekPertanyaan = false;
    TextView txtSoal;
    TextView  txtNo;
    int urutanPertanyaan = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuis);

        txtSoal = findViewById(R.id.textViewSoal);
        txtNo = findViewById(R.id.textViewHalaman);
        btnSelesai = findViewById(R.id.buttonSelesai);
        btnA = findViewById(R.id.jwb_A);
        btnB = findViewById(R.id.jwb_B);
        btnC = findViewById(R.id.jwb_C);
        btnD = findViewById(R.id.jwb_D);

        btnSelesai.setOnClickListener(klikSelesai);
        btnA.setOnClickListener(jawabA);
        btnB.setOnClickListener(jawabB);
        btnC.setOnClickListener(jawabC);
        btnD.setOnClickListener(jawabD);

        listSoal = new Database(this).getSoal();

        //new GetSoal().execute();
        jawabanYgDiPilih = new int[listSoal.size()];
        java.util.Arrays.fill(jawabanYgDiPilih, -1);
        jawabanYgBenar = new int[listSoal.size()];
        java.util.Arrays.fill(jawabanYgBenar, -1);

        setUpSoal();
    }

    private void setUpSoal() {
        this.tunjukanPertanyaan(0);
    }

    private void tunjukanPertanyaan(int urutan_soal_soal) {
        try {
            ModelKuis soal ;
            soal = listSoal.get(urutan_soal_soal);
            String pertanyaan = soal.getSoal();
            if (jawabanYgBenar[urutan_soal_soal] == -1) {
                jawabanYgBenar[urutan_soal_soal] = soal.getKunci();
            }

            txtSoal.setText(pertanyaan.toCharArray(), 0, pertanyaan.length());

            String jwb_a = soal.getPilA();
            btnA.setText(jwb_a.toCharArray(), 0,
                    jwb_a.length());
            String jwb_b = soal.getPilB();
            btnB.setText(jwb_b.toCharArray(), 0,
                    jwb_b.length());
            String jwb_c = soal.getPilC();
            btnC.setText(jwb_c.toCharArray(), 0,
                    jwb_c.length());
            String jwb_d = soal.getPilD();
            btnD.setText(jwb_d.toCharArray(), 0,
                    jwb_d.length());

            Log.d("", jawabanYgDiPilih[urutan_soal_soal] + "");

            pasangLabelDanNomorUrut();

        } catch (Exception e) {
            Log.e(this.getClass().toString(), e.getMessage(), e.getCause());
        }
    }

    private void pasangLabelDanNomorUrut() {
        txtNo.setText("  Soal ke-" + (urutanPertanyaan + 1) + " dari "
                + listSoal.size());
    }

    private void aturJawaban_nya() {


        Log.d("", Arrays.toString(jawabanYgDiPilih));
        Log.d("", Arrays.toString(jawabanYgBenar));

    }

    private final View.OnClickListener klikSelesai = v -> {
        aturJawaban_nya();
        // hitung berapa yg benar
        nilai();
    };

    public  void  selesai(){
        final AlertDialog dialog = new  AlertDialog.Builder(Kuis.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog,null);
        dialog.setView(dialogView);
        TextView text = dialogView.findViewById(R.id.tv_desc);
        text.setText("Apa Anda Ingin Keluar ?");
        Button dialogButton = dialogView.findViewById(R.id.bt_ok);
        Button ulangButton = dialogView.findViewById(R.id.bt_ulang);


        // Set the custom layout as alert dialog view

        dialogButton.setOnClickListener(v -> {
            cekPertanyaan = false;
            nilai();
            dialog.dismiss();
        });
        ulangButton.setOnClickListener(v -> dialog.dismiss());

        dialog.show();


    }

    public  void  nilai(){
        int jumlahJawabanYgBenar = 0;
        for (int i = 0; i < jawabanYgBenar.length; i++) {
            if ((jawabanYgBenar[i] != -1) && (jawabanYgBenar[i] == jawabanYgDiPilih[i]))
                jumlahJawabanYgBenar++;
        }
        final AlertDialog dialog = new  AlertDialog.Builder(Kuis.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.score_layout,null);
        dialog.setView(dialogView);
        dialog.setCancelable(false);
        TextView text = dialogView.findViewById(R.id.tv_desc);
        text.setText(""+jumlahJawabanYgBenar*10);
        Button dialogButton = dialogView.findViewById(R.id.bt_ok);
        Button ulangButton = dialogView.findViewById(R.id.bt_ulang);


        // Set the custom layout as alert dialog view

        dialogButton.setOnClickListener(v -> {
            cekPertanyaan = false;
            finish();
        });
        ulangButton.setOnClickListener(v -> {
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        });

        dialog.show();
/*        dialog.setMessage()
                    .setPositiveButton("Ulang", null)
                    .setNegativeButton("Tidak", null)
                .setPositiveButton("Ulang",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        cekPertanyaan = false;
                        finish();
                    }
                }).show();*/
    }


    public void next(){
        if (urutanPertanyaan == listSoal.size()-1) {
            nilai();
        } else {
            urutanPertanyaan++;

            if (urutanPertanyaan >= listSoal.size())
                urutanPertanyaan = listSoal.size() - 1;

            tunjukanPertanyaan(urutanPertanyaan);
        }
    }

    @Override
    public void onBackPressed() {
        selesai();
    }

    private final View.OnClickListener jawabA = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            jawabanYgDiPilih[urutanPertanyaan] = 0;
            next();
        }
    };

    private final View.OnClickListener jawabB = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            jawabanYgDiPilih[urutanPertanyaan] = 1;
            next();
        }
    };

    private final View.OnClickListener jawabC= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            jawabanYgDiPilih[urutanPertanyaan] = 2;
            next();
        }
    };

    private final View.OnClickListener jawabD = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            jawabanYgDiPilih[urutanPertanyaan] = 3;
            next();
        }
    };
}
