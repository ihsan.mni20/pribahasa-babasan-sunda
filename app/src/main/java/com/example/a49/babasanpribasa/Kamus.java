package com.example.a49.babasanpribasa;


import android.app.ProgressDialog;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.view.MenuItem;

import com.example.a49.babasanpribasa.Database.Database;
import com.example.a49.babasanpribasa.Model.ModelKamus;
import com.example.a49.babasanpribasa.ViewHolder.KamusAdater;

import java.util.ArrayList;
import java.util.List;

public class Kamus extends AppCompatActivity implements SearchView.OnQueryTextListener {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    List<ModelKamus> kamus = new ArrayList<>();
    ProgressDialog pd;
    KamusAdater adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kamus);

        pd = new ProgressDialog(this);
        //Init
        recyclerView = findViewById(R.id.listKamus);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        kamus = new Database(this).getKalimat();
        adapter = new KamusAdater(kamus, this);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.search_view, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setQueryHint("Cari... ");
        searchView.setIconified(false);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        pd.setMessage("Loading... ");
        pd.setCancelable(false);

        recyclerView = findViewById(R.id.listKamus);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        kamus = new Database(this).getSearch(newText);
        adapter = new KamusAdater(kamus, this);
        recyclerView.setAdapter(adapter);

        return true;
    }
}
