package com.example.a49.babasanpribasa.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.example.a49.babasanpribasa.Model.ModelKamus;
import com.example.a49.babasanpribasa.Model.ModelKuis;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 49 on 06/03/2018.
 */

public class Database extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "DBdatababas.db";
    private static final int DAATABASE_VERSION = 1;

    public Database(Context context){
        super(context, DATABASE_NAME, null, DAATABASE_VERSION);
    }

    public List<ModelKamus> getKalimat()
    {
        SQLiteDatabase db = getWritableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String[] sqlSelect={"Babas", "Makna"};
        String sqlTable = "Kamus";

        qb.setTables(sqlTable);
        Cursor c = qb.query(db, sqlSelect, null, null, null,null,"Babas");

        final List<ModelKamus> result = new ArrayList<>();
        if(c.moveToFirst())
        {
            do{
                result.add(new ModelKamus(c.getString(c.getColumnIndex("Babas")),
                        c.getString(c.getColumnIndex("Makna"))));
            }while (c.moveToNext());
        }
        return result;
    }

    public List<ModelKamus> getSearch(String cari)
    {
        SQLiteDatabase db = getWritableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String[] sqlSelect={"Babas", "Makna"};
        String sqlTable = "Kamus";

        qb.setTables(sqlTable);
        Cursor c = qb.query(db, sqlSelect, "Babas LIKE '%"+cari+"%'",null,null,null, "Babas");

        final List<ModelKamus> result = new ArrayList<>();
        if(c.moveToFirst())
        {
            do{
                result.add(new ModelKamus(c.getString(c.getColumnIndex("Babas")),
                        c.getString(c.getColumnIndex("Makna"))));
            }while (c.moveToNext());
        }
        return result;
    }

    public List<ModelKuis> getSoal()
    {
        SQLiteDatabase db = getWritableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String[] sqlSelect={"Soal","pilA","pilB","pilC","pilD","Kunci"};
        String sqlTable = "Kuis";

        qb.setTables(sqlTable);
        Cursor c = qb.query(db, sqlSelect, null,null,null,null, "random()", "10");

        final List<ModelKuis> result = new ArrayList<>();
        if(c.moveToFirst())
        {
            do{
                result.add(new ModelKuis(
                        c.getString(c.getColumnIndex("Soal")),
                        c.getString(c.getColumnIndex("pilA")),
                        c.getString(c.getColumnIndex("pilB")),
                        c.getString(c.getColumnIndex("pilC")),
                        c.getString(c.getColumnIndex("pilD")),
                        c.getInt(c.getColumnIndex("Kunci"))));
            }while (c.moveToNext());
        }
        return result;
    }
}