package com.example.a49.babasanpribasa.Model;

/**
 * Created by 49 on 06/03/2018.
 */

public class ModelKuis {
    private String Soal;
    private int Kunci;
    private String pilA;
    private String pilB;
    private String pilC;
    private String pilD;

    public ModelKuis() {
    }

    public ModelKuis(String soal, String pilA, String pilB, String pilC, String pilD, int kunci) {
        Soal = soal;
        Kunci = kunci;
        this.pilA = pilA;
        this.pilB = pilB;
        this.pilC = pilC;
        this.pilD = pilD;
    }

    public String getSoal() {
        return Soal;
    }

    public void setSoal(String soal) {
        Soal = soal;
    }

    public int getKunci() {
        return Kunci;
    }

    public void setKunci(int kunci) {
        Kunci = kunci;
    }

    public String getPilA() {
        return pilA;
    }

    public void setPilA(String pilA) {
        this.pilA = pilA;
    }

    public String getPilB() {
        return pilB;
    }

    public void setPilB(String pilB) {
        this.pilB = pilB;
    }

    public String getPilC() {
        return pilC;
    }

    public void setPilC(String pilC) {
        this.pilC = pilC;
    }

    public String getPilD() {
        return pilD;
    }

    public void setPilD(String pilD) {
        this.pilD = pilD;
    }
}


