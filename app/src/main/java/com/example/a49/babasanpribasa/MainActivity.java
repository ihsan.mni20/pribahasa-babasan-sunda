package com.example.a49.babasanpribasa;

import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import info.hoang8f.widget.FButton;

public class MainActivity extends AppCompatActivity {

    FButton btnKamus, btnKuis, btnTentang, btnKeluar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnKamus = findViewById(R.id.btn_kamus);
        btnKuis = findViewById(R.id.btn_kuis);
        btnTentang = findViewById(R.id.btn_tentang);
        btnKeluar = findViewById(R.id.btn_keluar);

        btnKamus.setOnClickListener(view -> {
            Intent kam = new Intent( MainActivity.this,Kamus.class);
            startActivity(kam);
        });

        btnKuis.setOnClickListener(view -> {
            Intent kuis = new Intent( MainActivity.this,Kuis.class);
            startActivity(kuis);
        });

        btnKeluar.setOnClickListener(view -> keluar());

        btnTentang.setOnClickListener(view -> {
            Intent ttg = new Intent( MainActivity.this,Tentang.class);
            startActivity(ttg);
        });

    }

    public void keluar(){
        final AlertDialog dialog = new  AlertDialog.Builder(MainActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog,null);
        dialog.setView(dialogView);
        TextView text = dialogView.findViewById(R.id.tv_desc);
        text.setText("Apa Ingin Keluar ?");
        Button dialogButton = dialogView.findViewById(R.id.bt_ok);
        Button ulangButton = dialogView.findViewById(R.id.bt_ulang);


        // Set the custom layout as alert dialog view

        dialogButton.setOnClickListener(v -> {
            finish();
            System.exit(1);
        });
        ulangButton.setOnClickListener(v -> dialog.dismiss());

        dialog.show();


    }

  /*  @Override
    public void onBackPressed() {
        keluar();
    }*/
}
