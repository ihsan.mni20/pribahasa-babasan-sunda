package com.example.a49.babasanpribasa;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Makna extends AppCompatActivity {
    TextView babas, makna;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makna);

        babas = findViewById(R.id.babas);
        makna = findViewById(R.id.makna);

        Intent data = getIntent();
         babas.setText(data.getStringExtra("babas"));
         makna.setText(data.getStringExtra("makna"));
    }


}
